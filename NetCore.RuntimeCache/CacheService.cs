﻿using fmtx.Framework.Cache;
using NetCore.IBLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NetCore.RuntimeCache
{
    /// <summary>
    /// 运行时缓存
    /// </summary>
    public class CacheService : ICache
    {
        /// <summary>
        /// 获取缓存对象
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <returns></returns>
        public T Get<T>(string key)
        {
            var cache = new DataCache(key);
            var obj = cache.Get();
            if (obj != null)
            {
                return (T)obj;
            }
            return default(T);
        }
        /// <summary>
        /// 获取缓存列表
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <returns></returns>
        public IList<T> GetList<T>(string key)
        {
            var cache = new DataCache(key);
            var obj = cache.Get();
            if (obj != null)
            {
                return (IList<T>)obj;
            }
            return null;
        }

        public List<string> Keys()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// 移除缓存
        /// </summary>
        /// <param name="key"></param>
        public void Remove(string key)
        {
            var cache = new DataCache(key);
            cache.Remove();
        }
        /// <summary>
        /// 设置缓存
        /// </summary>
        /// <param name="key"></param>
        /// <param name="data"></param>
        public void Set<T>(string key, T data)
        {
            var cache = new DataCache(key);
            cache.SetByTime(data, 30);
        }
        /// <summary>
        /// 设置定时缓存
        /// </summary>
        /// <param name="key"></param>
        /// <param name="data"></param>
        /// <param name="timeout"></param>
        public void Set<T>(string key, T data, int timeout)
        {
            var cache = new DataCache(key);
            cache.SetByTime(data, timeout);
        }
    }
}
