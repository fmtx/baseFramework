﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NetCore.IBLL
{
    /// <summary>
    /// 业务实体核心结构规范接口
    /// </summary>
    public interface IEntityBase
    {
        Guid ID { get; set; }  // 业务实体对象ID
    }
}
