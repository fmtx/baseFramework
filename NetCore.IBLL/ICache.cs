﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NetCore.IBLL
{
    public interface ICache
    {
        /// <summary>
        /// 获取缓存数据
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        IList<T> GetList<T>(string key);
        /// <summary>
        /// 获取缓存数据
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        T Get<T>(string key);
        /// <summary>
        /// 写入缓存
        /// </summary>
        /// <param name="key"></param>
        /// <param name="data"></param>
        void Set<T>(string key, T data);
        /// <summary>
        /// 写入缓存定时清除
        /// </summary>
        /// <param name="key"></param>
        /// <param name="data"></param>
        /// <param name="timeout"></param>
        void Set<T>(string key, T data, int timeout);
        /// <summary>
        /// 清除缓存
        /// </summary>
        /// <param name="key"></param>
        void Remove(string key);
        /// <summary>
        /// 获取所有键集合
        /// </summary>
        /// <returns></returns>
        List<string> Keys();
    }
}
