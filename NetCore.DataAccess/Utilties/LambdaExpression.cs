﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetCore.DataAccess.Utilties
{
    public static class LambdaExpression
    {
        public static IList<T> ToPaginatedList<T>(this IQueryable<T> query, int index, int size)
        {
            return query.Skip((index - 1) * size).Take(size).ToList();
        }
    }
}
