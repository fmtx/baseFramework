﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using fmtx.Framework.DESC;

namespace NetCore.Common.Web
{
    public class WebCookieHelper
    {
        #region 清除Cookie
        /// <summary>
        /// 清除Cookie
        /// </summary>
        /// <param name="domain"></param>
        /// <param name="key"></param>
        public static void ClearCookie(Microsoft.AspNetCore.Http.HttpContext context,string domain, string key)
        {
            SetCookie(context,domain, key, string.Empty, 0.0);
        }
        #endregion

        #region 写入加密Cookie
        /// <summary>
        /// 写入加密Cookie
        /// </summary>
        /// <param name="domain">加密密钥</param>
        /// <param name="key">cookie键</param>
        /// <param name="value">需要写入的值</param>
        /// <param name="expired">保存时间（小时）</param>
        public static void SetCookie(Microsoft.AspNetCore.Http.HttpContext context,string domain, string key, string encrypt, string value, double expired = 0)
        {
            DateTime Expires;
            if (expired > 0.0)
            {
                Expires = DateTime.Now.AddHours(expired);
            }
            else
            {
                Expires = DateTime.MinValue;
            }
            context.Response.Cookies.Append(DESCHandler.Encrypt(key, encrypt), value, new Microsoft.AspNetCore.Http.CookieOptions
            {
                Expires = Expires,
                Domain=domain
            });
        }
        #endregion

        #region 写入Cookie
        /// <summary>
        /// 写入Cookie
        /// </summary>
        /// <param name="key"></param>
        /// <param name="encrypt"></param>
        /// <param name="value"></param>
        /// <param name="expired"></param>
        public static void SetCookie(Microsoft.AspNetCore.Http.HttpContext context,string key, string encrypt, string value, double expired = 0)
        {
            DateTime Expires;
            if (expired > 0.0)
            {
                Expires = DateTime.Now.AddHours(expired);
            }
            else
            {
                Expires = DateTime.MinValue;
            }
            context.Response.Cookies.Append(DESCHandler.Encrypt(key, encrypt), value, new Microsoft.AspNetCore.Http.CookieOptions
            {
                Expires = Expires
            });
        }
        #endregion

        #region 写入Cookie
        /// <summary>
        /// 写入Cookie
        /// </summary>
        /// <param name="key">键</param>
        /// <param name="value">值</param>
        public static void SetCookie(Microsoft.AspNetCore.Http.HttpContext context,string key, string value, double expired = 0)
        {
            context.Response.Cookies.Append(key, value, new Microsoft.AspNetCore.Http.CookieOptions {
                Expires= DateTime.Now.AddMinutes(expired)
            });
        }
        #endregion

        #region 移除cookie
        /// <summary>
        /// 移除cookie
        /// </summary>
        /// <param name="key"></param>
        public static void RemoveCookie(Microsoft.AspNetCore.Http.HttpContext context)
        {
            string cookieName;
            int limit = context.Request.Cookies.Count;
            var klist = context.Request.Cookies.Keys.ToList();
            for (int i = 0; i < limit; i++)
            {
                try
                {
                    cookieName = klist[i];
                    context.Response.Cookies.Delete(cookieName);
                }
                catch (Exception)
                {
                    continue;
                }
            }
        }
        #endregion

        #region 获取用户cookie
        /// <summary>
        /// 获取用户cookie
        /// </summary>
        /// <param name="key">键</param>
        /// <returns>返回值</returns>
        public static string GetCookie(Microsoft.AspNetCore.Http.HttpContext context,string key)
        {
            var result = "";
            if (context.Request.Cookies.TryGetValue(key, out result))
            {
                if (!string.IsNullOrEmpty(result))
                    return result;
            }
            return string.Empty;
        }
        #endregion

        #region 获取解密后的cookie数据
        /// <summary>
        /// 获取解密后的cookie数据
        /// </summary>
        /// <param name="key">键</param>
        /// <param name="decrypt">解密密钥</param>
        /// <returns>解密后数据</returns>
        public static string GetCookie(Microsoft.AspNetCore.Http.HttpContext context, string key, string decrypt)
        {
            key = DESCHandler.Encrypt(key, decrypt);
            var result = "";
            if(context.Request.Cookies.TryGetValue(key, out result))
            {
                if (!string.IsNullOrEmpty(result))
                    return DESCHandler.Decrypt(result, decrypt);
            }
            return string.Empty;
        }
        #endregion
    }
}
