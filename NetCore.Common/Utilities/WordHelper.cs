﻿using NPOI.XWPF.UserModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetCore.Common.Utilities
{
    public class WordHelper
    {

        /// <summary>
        /// word转为Pdf预览
        /// </summary>
        /// <param name="filePath">文件路径（绝对路径包括文件名）</param>
        /// <param name="basePath">虚拟目录名称</param>
        /// <returns></returns>
        public static string Word2Pdf(string filePath)
        {
            try
            {
                var fileDir = Path.GetDirectoryName(filePath) + @"\viewTemp\";
                var fileName = Path.GetFileName(filePath);
                // Create a temporary folder
                string documentFolder = fileDir + fileName;
                if (!Directory.Exists(documentFolder))
                {
                    Directory.CreateDirectory(documentFolder);
                }

                // Load the document in Aspose.Words
                Aspose.Words.Document doc = new Aspose.Words.Document(filePath);
                //直接转换为pdf文件
                doc.Save(string.Format(@"{0}\{1}.pdf", documentFolder, 0), Aspose.Words.SaveFormat.Pdf);
                return fileName;
            }
            catch (Exception)
            {

                throw new Exception("格式不正确！无法完成预览！");
            }
        }

        /// <summary>
        /// 读取Word纯文本内容
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public static string ReadWordText(string fileName)
        {
            string fileText = string.Empty;

            try
            {
                Aspose.Words.Document doc = new Aspose.Words.Document(fileName);
                fileText = doc.ToString(Aspose.Words.SaveFormat.Text);
            }
            catch (Exception)
            {
                #region 另一种方式打开文档
                StringBuilder sbFileText = new StringBuilder();
                XWPFDocument document = null;
                try
                {
                    using (FileStream file = new FileStream(fileName, FileMode.Open, FileAccess.Read))
                    {
                        document = new XWPFDocument(file);
                    }
                    //正文段落
                    foreach (XWPFParagraph paragraph in document.Paragraphs)
                    {
                        sbFileText.AppendLine(paragraph.ParagraphText);
                    }

                    fileText = sbFileText.ToString();
                }
                catch (Exception)
                {
                    throw new Exception("无法读取文档！！请检查文档格式是否正确！！");
                }
                #endregion
            }
            return fileText;
        }

        /// <summary>
        /// 将html保存为word
        /// </summary>
        /// <param name="path"></param>
        /// <param name="html"></param>
        /// <returns></returns>
        public static bool Save2Word(string path, string html)
        {
            if (File.Exists(path))
            {
                File.Delete(path);
            }
            using (StreamWriter sw = new StreamWriter(path, false, Encoding.UTF8))
            {
                sw.WriteLine(html);
            }
            return true;
        }
    }
}
