﻿using fmtx.Framework.DALFactory;
using NetCore.IBLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetCore.Common.Utilities
{
    public class DataCache : ICache
    {
        private readonly ICache cache;
        public DataCache()
        {
            cache = FactoryService.GetObject<ICache>();
        }

        /// <summary>
        /// 获取缓存数据
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <returns></returns>
        public T Get<T>(string key)
        {
            return cache.Get<T>(key);
        }
        /// <summary>
        /// 获取缓存列表
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <returns></returns>
        public IList<T> GetList<T>(string key)
        {
            return cache.GetList<T>(key);
        }

        public List<string> Keys()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// 写入缓存
        /// </summary>
        /// <param name="key"></param>
        /// <param name="data"></param>
        public void Remove(string key)
        {
            cache.Remove(key);
        }
        /// <summary>
        /// 写入缓存定时清除
        /// </summary>
        /// <param name="key"></param>
        /// <param name="data"></param>
        /// <param name="timeout"></param>
        public void Set<T>(string key, T data)
        {
            cache.Set(key, data);
        }
        /// <summary>
        /// 清除缓存
        /// </summary>
        /// <param name="key"></param>
        public void Set<T>(string key, T data, int timeout)
        {
            cache.Set(key, data, timeout);
        }
    }
}
