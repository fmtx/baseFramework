﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetCore.Common.Utilities
{
    /// <summary>
    /// 一些扩展的简单的字符串操作方法
    /// </summary>
    public static class BusinessStringHelper
    {
        /// <summary>
        /// 将布尔值转换为汉字：是/否
        /// </summary>
        /// <param name="tfValue"></param>
        /// <returns></returns>
        public static string BooleanToYesNo(bool tfValue)
        {
            var result = "";
            if (tfValue)
                result = "是";
            else
                result = "否";

            return result;
        }

        /// <summary>
        /// 将布尔值转换为汉字：男/女
        /// </summary>
        /// <param name="tfValue"></param>
        /// <returns></returns>
        public static string BooleanToSex(bool tfValue)
        {
            var result = "";
            if (tfValue)
                result = "男";
            else
                result = "女";

            return result;

        }

        /// <summary>
        /// 剔除字符串中 Html 标识符
        /// </summary>
        /// <param name="source"></param>
        /// <param name="length">如果为零，则返回全部，否则返回指定的长度</param>
        /// <returns></returns>
        public static string HtmlToText(string source, int length = 0)
        {
            string strNohtml = System.Text.RegularExpressions.Regex.Replace(source, "<[^>]+>", "");
            strNohtml = System.Text.RegularExpressions.Regex.Replace(strNohtml, "&[^;]+;", "");

            if (length > 0)
            {
                if (strNohtml.Length > length - 2)
                    strNohtml = strNohtml.Substring(0, length - 2) + "...";
            }
            return strNohtml;
        }

        /// <summary>
        /// 过滤换行符
        /// </summary>
        /// <param name="source"></param>
        /// <param name="length">如果为零，则返回全部，否则返回指定的长度</param>
        /// <returns></returns>
        public static string HtmlToFilterTabSymbols(string source, int length = 0)
        {
            string strNohtml = System.Text.RegularExpressions.Regex.Replace(source, "\r?\n", "");
            strNohtml = System.Text.RegularExpressions.Regex.Replace(strNohtml, " +<", "<");
            if (length > 0)
            {
                if (strNohtml.Length > length - 2)
                    strNohtml = strNohtml.Substring(0, length - 2) + "...";
            }
            return strNohtml;
        }

        /// <summary>
        /// 普通字符串省略
        /// </summary>
        /// <param name="source"></param>
        /// <param name="length">如果为零，则返回全部，否则返回指定的长度</param>
        /// <returns></returns>
        public static string TextEllipsis(string source, int length = 0)
        {
            if (length <= 0) return source;
            if (source.Length > length - 2)
                source = source.Substring(0, length - 2) + "...";
            return source;
        }

    }
}
