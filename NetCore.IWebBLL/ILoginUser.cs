﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NetCore.IWebBLL
{
    public interface ILoginUser
    {
        DateTime TimeOut { get; set; }
    }
}
