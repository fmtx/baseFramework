﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NetCore.IWebBLL
{
    public interface ISession
    {
        /// <summary>
        /// 保存登录信息并返回token
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        Guid LogToken(ILoginUser user);
        /// <summary>
        /// 通过token获取登录用户信息
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        T Get<T>(Guid token) where T : ILoginUser;
        /// <summary>
        /// 更新数据
        /// </summary>
        /// <param name="token"></param>
        /// <param name="bean"></param>
        void Update(Guid token, ILoginUser bean);
        /// <summary>
        /// 移除登录信息
        /// </summary>
        /// <param name="token"></param>
        void Remove(Guid token);
        /// <summary>
        /// 清除登录超时的
        /// </summary>
        void Clear();
        /// <summary>
        /// 更新超时时间
        /// </summary>
        void UpdateTime(Guid token);
        /// <summary>
        /// 获取存储对象
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <returns></returns>
        T GetData<T>(string key);
        /// <summary>
        /// 保存对象
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <param name="data"></param>
        void SetData<T>(string key, T data);
        /// <summary>
        /// 移除数据
        /// </summary>
        /// <param name="key"></param>
        void RemoveData(string key);
    }
}
